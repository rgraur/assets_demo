# Install
Run all commands from project root.

### Node modules: RequireJS & Bower
`$ node install requirejs bower`

### Javascript libraries: jQuery, RequireJS, Backbone, Underscore
`$ node_modules/bower/bin/bower install`

### Build
Will create `prod/` folder in the project root.

`$ dev/build/build.sh`