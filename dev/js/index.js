// paths to dependencies
require.config({
  'paths': {
    'jquery':     'vendor/jquery/jquery',
    'backbone':   'vendor/backbone-amd/backbone',
    'underscore': 'vendor/underscore-amd/underscore'
  }
});

/**
 * Initialize App view.
 *
 * @param  {object} AppView application view
 */
require(['views/app'], function(AppView){
  var appView = new AppView;

  // test public and private variables
  $('#box').append('<div>' + appView.publicVar + '</div>');
  $('#box').append('<div>' + appView.privateVar + '</div>');
  $('#box').append('<div>' + appView.getPrivate() + '</div>');
});