/**
 * App module.
 *
 * Dependencies:
 * - backbone;
 *
 * @param  {object} Backbone BackboneJS instance
 * @return {object}          application view object
 */
define(['backbone'], function(Backbone){
  var App = Backbone.View.extend({
    /**
     * Init view (constructor).
     */
    initialize: function(){
      var privateVar = 'i am private';
      this.publicVar = 'i am public';

      /**
       * Getter for privateVar.
       *
       * @return {string} private var
       */
      this.getPrivate = function(){
        return privateVar;
      };
    }
  });

  return App;
});