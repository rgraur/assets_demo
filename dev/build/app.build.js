({
  appDir:         '../',
  baseUrl:        'js/',
  dir:            '../../prod',
  mainConfigFile: '../js/index.js',
  name:           'index',
  optimizeCss:    'standard'
})