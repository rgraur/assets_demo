# optimize scripts
node_modules/requirejs/bin/r.js -o dev/build/app.build.js

# remove unused files and folders
rm -rf prod/build/ prod/build.txt prod/js/views prod/js/vendor/backbone-amd/ prod/js/vendor/jquery/ prod/js/vendor/underscore-amd/ prod/css/generic.css